<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\CiudadSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Ciudades');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ciudad-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
	<?php if($userCan['ciudad-create']): ?>
    <p>
        <?= Html::a(Yii::t('app', 'Crear Ciudad'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
	<?php endif; ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'id',
            ['attribute'=>'pais_id','value'=>function($data){return $data->pais->nombre;},'filter'=>$paises],
            'pais_cd',
            ['attribute'=>'region_id','value'=>function($data){return $data->region->nombre;},'filter'=>$regiones],
            'region_cd',
            'nombre',
            // 'created_at',
            // 'updated_at',
            // 'created_by',
            // 'updated_by',
            'status',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
