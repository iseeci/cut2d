<?php

namespace frontend\modules\geo;

class Geo extends \yii\base\Module
{
    public $controllerNamespace = 'frontend\modules\geo\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
