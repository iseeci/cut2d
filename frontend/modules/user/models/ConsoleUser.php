<?php
namespace frontend\modules\user\models;

use Yii;
use dektrium\user\models\User;
use yii\log\Logger;
use dektrium\user\models\Token;

class ConsoleUser extends User{
	public $code;
	public function register(){
		if ($this->getIsNewRecord() == false) {
            throw new \RuntimeException('Calling "' . __CLASS__ . '::' . __METHOD__ . '" on existing user');
        }

        if ($this->module->enableConfirmation == false) {
            $this->confirmed_at = time();
        }

        if ($this->module->enableGeneratingPassword) {
            $this->password = Password::generate(8);
        }

        $this->trigger(self::USER_REGISTER_INIT);

        if ($this->save()) {
            $this->trigger(self::USER_REGISTER_DONE);
            if ($this->module->enableConfirmation) {
                $token = \Yii::createObject([
                    'class' => Token::className(),
                    'type'  => Token::TYPE_CONFIRMATION,
                ]);
                $token->link('user', $this);
				$this->code=$token->code;
                //$this->mailer->sendConfirmationMessage($this, $token);
            }/* else {
                \Yii::$app->user->login($this);
            }
			
            if ($this->module->enableGeneratingPassword) {
                $this->mailer->sendWelcomeMessage($this);
            }
			*/
            //\Yii::$app->session->setFlash('info', $this->getFlashMessage());
			
            \Yii::getLogger()->log('User has been registered', Logger::LEVEL_INFO);
            return true;
        }

        \Yii::getLogger()->log('An error occurred while registering user account', Logger::LEVEL_ERROR);

        return false;
	}	

    public function attemptConfirmation($code)
    {
        /** @var Token $token */
        $token = $this->finder->findToken([
            'user_id' => $this->id,
            'code'    => $code,
            'type'    => Token::TYPE_CONFIRMATION,
        ])->one();

        if ($token !== null && !$token->isExpired){
            $token->delete();
    
            $this->confirmed_at = time();
    
            \Yii::getLogger()->log('User has been confirmed', Logger::LEVEL_INFO);
    
            $this->save(false);
        }
    }
}
