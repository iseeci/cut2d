<?php

use yii\helpers\Html;
use kartik\form\ActiveForm;
use frontend\assets\GeoAsset;
/* @var $this yii\web\View */
/* @var $model frontend\models\Direccion */
/* @var $form ActiveForm */
?>
<?php
GeoAsset::register($this,[
	'pais_field'=>'firmaingeniero-pais_id',
	'region_field'=>'firmaingeniero-region_id',
	'ciudad_field'=>'firmaingeniero-ciudad_id',
	'pais_id'=>$model->pais_id,
	'region_id'=>$model->region_id,
	'ciudad_id'=>$model->ciudad_id]);
?>

<div class="poliza-certificado-_formDireccion">
<?php if(!$form): ?> 
    <?php $form = ActiveForm::begin(); ?>
<?php endif; ?>
        <?= $form->field($model, 'pais_id')->dropDownList($opcionesPaises,['prompt'=>'Escoge país']) ?>
        <?= $form->field($model, 'region_id')->dropDownList([],['prompt'=>'Escoge primero país']) ?>
        <?= $form->field($model, 'ciudad_id')->dropDownList([],['prompt'=>'Escoge primero región']) ?>
<?php if(!$form): ?>   
        <div class="form-group">
            <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>
<?php endif; ?>
</div><!-- poliza-certificado-_formDireccion -->
