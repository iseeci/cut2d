<?php

/* 
 * This file is part of the Dektrium project
 * 
 * (c) Dektrium project <http://github.com/dektrium>
 * 
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

use yii\bootstrap\Nav;

?>

<?= Nav::widget([
    'options' => [
        'class' => 'nav-tabs',
        'style' => 'margin-bottom: 15px'
    ],
    'items' => [
        [
            'label'   => Yii::t('user', 'Users'),
            'url'     => ['/user/admin/index'],
        ],/*
        [
            'label' => Yii::t('user', 'Roles'),
            'url'   => ['/rbac/role/index'],
            'visible' => isset(Yii::$app->extensions['dektrium/yii2-rbac']),
        ],
        [
            'label' => Yii::t('user', 'Permissions'),
            'url'   => ['/rbac/permission/index'],
            'visible' => isset(Yii::$app->extensions['dektrium/yii2-rbac']),
        ],*/
        [
            'label' => Yii::t('user', 'Crear'),
            'items' => [
                [
                    'label'   => Yii::t('user', 'Nuevo admin'),
                    'url'     => ['/user/admin/create','role'=>'admin'],
                ],
                [
                    'label'   => Yii::t('user', 'Nuevo manager'),
                    'url'     => ['/user/admin/create','role'=>'manager'],
                ],
                [
                    'label'   => Yii::t('user', 'Nuevo ingeniero'),
                    'url'     => ['/user/admin/create','role'=>'ingeniero'],
                ],
				/*
                [
                    'label' => Yii::t('user', 'New role'),
                    'url'   => ['/rbac/role/create'],
                    'visible' => isset(Yii::$app->extensions['dektrium/yii2-rbac']),
                ],
                [
                    'label' => Yii::t('user', 'New permission'),
                    'url'   => ['/rbac/permission/create'],
                    'visible' => isset(Yii::$app->extensions['dektrium/yii2-rbac']),
                ]*/
            ]
        ]
    ]
]) ?>
