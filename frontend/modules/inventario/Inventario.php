<?php

namespace frontend\modules\inventario;

class Inventario extends \yii\base\Module
{
    public $controllerNamespace = 'frontend\modules\inventario\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
