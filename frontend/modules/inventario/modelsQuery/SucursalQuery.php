<?php

namespace frontend\modules\inventario\modelsQuery;

/**
 * This is the ActiveQuery class for [[Sucursal]].
 *
 * @see Sucursal
 */
class SucursalQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return Sucursal[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Sucursal|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}