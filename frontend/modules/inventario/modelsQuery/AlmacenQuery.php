<?php

namespace frontend\modules\inventario\modelsQuery;

/**
 * This is the ActiveQuery class for [[Almacen]].
 *
 * @see Almacen
 */
class AlmacenQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return Almacen[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Almacen|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}