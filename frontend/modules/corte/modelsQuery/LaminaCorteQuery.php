<?php

namespace frontend\modules\corte\modelsQuery;

/**
 * This is the ActiveQuery class for [[\frontend\modules\corte\models\LaminaCorte]].
 *
 * @see \frontend\modules\corte\models\LaminaCorte
 */
class LaminaCorteQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return \frontend\modules\corte\models\LaminaCorte[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \frontend\modules\corte\models\LaminaCorte|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}