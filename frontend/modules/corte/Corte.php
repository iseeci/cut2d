<?php

namespace frontend\modules\corte;

class Corte extends \yii\base\Module
{
    public $controllerNamespace = 'frontend\modules\corte\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
