<?php

use frontend\components\batchformwidget\BatchFormWidget;
use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\modules\corte\models\Lamina */

$this->title = Yii::t('app', 'Ingesar Láminas');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Laminas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="lamina-create-batch">

    <h1><?= Html::encode($this->title) ?></h1>
	
	<?= BatchFormWidget::widget(['models'=>$model,'rowViewPath'=>$rowViewPath,'rowViewParams'=>[]]); ?>

</div>
