<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\modules\corte\models\Lamina */

$this->title = Yii::t('app', 'Editar {modelClass}: ', [
    'modelClass' => 'Lamina',
]) . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Laminas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="lamina-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
