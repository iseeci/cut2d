<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\modules\corte\models\Lamina */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Laminas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="lamina-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Editar'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Eliminar'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Estás seguro de querer eliminar este registro?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'material_id',
            'grosor_id',
            'area',
            'nueva',
            'programada',
            'sucursal_id',
            'almacen_id',
            'created_at',
            'updated_at',
            'created_by',
            'updated_by',
            'status',
            'forma',
        ],
    ]) ?>

</div>
