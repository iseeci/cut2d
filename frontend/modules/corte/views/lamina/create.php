<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\modules\corte\models\Lamina */

$this->title = Yii::t('app', 'Crear Lamina');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Laminas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="lamina-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'materiales' => $materiales,
        'grosores' => $grosores,
    ]) ?>

</div>
