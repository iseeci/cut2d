<?php

namespace frontend\modules\corte\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\modules\corte\models\Lamina;

/**
 * LaminaSearch represents the model behind the search form about `frontend\modules\corte\models\Lamina`.
 */
class LaminaSearch extends Lamina
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'material_id', 'grosor_id', 'nueva', 'programada', 'sucursal_id', 'almacen_id', 'created_by', 'updated_by'], 'integer'],
            [['area'], 'number'],
            [['created_at', 'updated_at', 'status', 'forma'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Lamina::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'material_id' => $this->material_id,
            'grosor_id' => $this->grosor_id,
            'area' => $this->area,
            'nueva' => $this->nueva,
            'programada' => $this->programada,
            'sucursal_id' => $this->sucursal_id,
            'almacen_id' => $this->almacen_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'forma', $this->forma]);

        return $dataProvider;
    }
}
