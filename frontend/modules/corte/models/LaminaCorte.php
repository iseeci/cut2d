<?php

namespace frontend\modules\corte\models;

use Yii;
use dektrium\user\models\User;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;

/**
 * This is the model class for table "lamina_corte".
 *
 * @property integer $id
 * @property integer $pedido_id
 * @property integer $pedido_lamina_id
 * @property integer $lamina_id
 * @property integer $material_id
 * @property integer $grosor_id
 * @property double $area
 * @property integer $nueva
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property string $status
 * @property string $forma
 *
 * @property User $createdBy
 * @property Grosor $grosor
 * @property Lamina $lamina
 * @property Material $material
 * @property Pedido $pedido
 * @property PedidoLamina $pedidoLamina
 * @property User $updatedBy
 * @property LaminaRetal[] $laminaRetals
 */
class LaminaCorte extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'lamina_corte';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pedido_id', 'pedido_lamina_id', 'lamina_id', 'material_id', 'grosor_id', 'area', 'nueva', 'created_at', 'created_by'], 'required'],
            [['pedido_id', 'pedido_lamina_id', 'lamina_id', 'material_id', 'grosor_id', 'nueva', 'created_by', 'updated_by'], 'integer'],
            [['area'], 'number'],
            [['created_at', 'updated_at'], 'safe'],
            [['forma'], 'string'],
            [['status'], 'string', 'max' => 255],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by' => 'id']],
            [['grosor_id'], 'exist', 'skipOnError' => true, 'targetClass' => Grosor::className(), 'targetAttribute' => ['grosor_id' => 'id']],
            [['lamina_id'], 'exist', 'skipOnError' => true, 'targetClass' => Lamina::className(), 'targetAttribute' => ['lamina_id' => 'id']],
            [['material_id'], 'exist', 'skipOnError' => true, 'targetClass' => Material::className(), 'targetAttribute' => ['material_id' => 'id']],
            [['pedido_id'], 'exist', 'skipOnError' => true, 'targetClass' => Pedido::className(), 'targetAttribute' => ['pedido_id' => 'id']],
            [['pedido_lamina_id'], 'exist', 'skipOnError' => true, 'targetClass' => PedidoLamina::className(), 'targetAttribute' => ['pedido_lamina_id' => 'id']],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['updated_by' => 'id']]
        ];
    }
	
	public function behaviors(){
		return [
			'timestamp' => [
	            'class' => TimestampBehavior::className(),
	            'attributes' => [
	                ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
	                ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
	            ],
	            'value' => new Expression('NOW()'),
             ],
			'blameable' => ['class' => BlameableBehavior::className(),],
        ];
	}

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'pedido_id' => Yii::t('app', 'Pedido ID'),
            'pedido_lamina_id' => Yii::t('app', 'Pedido Lamina ID'),
            'lamina_id' => Yii::t('app', 'Lamina ID'),
            'material_id' => Yii::t('app', 'Material ID'),
            'grosor_id' => Yii::t('app', 'Grosor ID'),
            'area' => Yii::t('app', 'Area'),
            'nueva' => Yii::t('app', 'Nueva'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'status' => Yii::t('app', 'Status'),
            'forma' => Yii::t('app', 'Forma'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGrosor()
    {
        return $this->hasOne(Grosor::className(), ['id' => 'grosor_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLamina()
    {
        return $this->hasOne(Lamina::className(), ['id' => 'lamina_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMaterial()
    {
        return $this->hasOne(Material::className(), ['id' => 'material_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPedido()
    {
        return $this->hasOne(Pedido::className(), ['id' => 'pedido_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPedidoLamina()
    {
        return $this->hasOne(PedidoLamina::className(), ['id' => 'pedido_lamina_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLaminaRetals()
    {
        return $this->hasMany(LaminaRetal::className(), ['lamina_corte_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return \frontend\modules\corte\modelsQuery\LaminaCorteQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \frontend\modules\corte\modelsQuery\LaminaCorteQuery(get_called_class());
    }
}
