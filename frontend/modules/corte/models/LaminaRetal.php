<?php

namespace frontend\modules\corte\models;

use Yii;
use dektrium\user\models\User;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;

/**
 * This is the model class for table "lamina_retal".
 *
 * @property integer $id
 * @property integer $lamina_corte_id
 * @property integer $lamina_id
 *
 * @property Lamina $lamina
 * @property LaminaCorte $laminaCorte
 */
class LaminaRetal extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'lamina_retal';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['lamina_corte_id', 'lamina_id'], 'required'],
            [['lamina_corte_id', 'lamina_id'], 'integer'],
            [['lamina_id'], 'exist', 'skipOnError' => true, 'targetClass' => Lamina::className(), 'targetAttribute' => ['lamina_id' => 'id']],
            [['lamina_corte_id'], 'exist', 'skipOnError' => true, 'targetClass' => LaminaCorte::className(), 'targetAttribute' => ['lamina_corte_id' => 'id']]
        ];
    }
	
	public function behaviors(){
		return [
			'timestamp' => [
	            'class' => TimestampBehavior::className(),
	            'attributes' => [
	                ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
	                ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
	            ],
	            'value' => new Expression('NOW()'),
             ],
			'blameable' => ['class' => BlameableBehavior::className(),],
        ];
	}

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'lamina_corte_id' => Yii::t('app', 'Lamina Corte ID'),
            'lamina_id' => Yii::t('app', 'Lamina ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLamina()
    {
        return $this->hasOne(Lamina::className(), ['id' => 'lamina_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLaminaCorte()
    {
        return $this->hasOne(LaminaCorte::className(), ['id' => 'lamina_corte_id']);
    }

    /**
     * @inheritdoc
     * @return \frontend\modules\corte\modelsQuery\LaminaRetalQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \frontend\modules\corte\modelsQuery\LaminaRetalQuery(get_called_class());
    }
}
