<?php

namespace frontend\modules\corte\models;

use Yii;
use dektrium\user\models\User;
use frontend\modules\inventario\models\Almacen;
use frontend\modules\inventario\models\Grosor;
use frontend\modules\inventario\models\Material;
use frontend\modules\inventario\models\Sucursal;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;

/**
 * This is the model class for table "lamina".
 *
 * @property integer $id
 * @property integer $material_id
 * @property integer $grosor_id
 * @property double $area
 * @property integer $nueva
 * @property integer $programada
 * @property integer $sucursal_id
 * @property integer $almacen_id
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property string $status
 * @property string $forma
 *
 * @property Almacen $almacen
 * @property User $createdBy
 * @property Grosor $grosor
 * @property Material $material
 * @property Sucursal $sucursal
 * @property User $updatedBy
 * @property LaminaCorte[] $laminaCortes
 * @property LaminaRetal[] $laminaRetals
 */
class Lamina extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'lamina';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['material_id', 'grosor_id', 'area', 'nueva', 'programada', 'sucursal_id', 'almacen_id', 'created_at', 'created_by'], 'required'],
            [['material_id', 'grosor_id', 'nueva', 'programada', 'sucursal_id', 'almacen_id', 'created_by', 'updated_by'], 'integer'],
            [['area'], 'number'],
            [['created_at', 'updated_at'], 'safe'],
            [['forma'], 'string'],
            [['status'], 'string', 'max' => 255],
            [['almacen_id'], 'exist', 'skipOnError' => true, 'targetClass' => Almacen::className(), 'targetAttribute' => ['almacen_id' => 'id']],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by' => 'id']],
            [['grosor_id'], 'exist', 'skipOnError' => true, 'targetClass' => Grosor::className(), 'targetAttribute' => ['grosor_id' => 'id']],
            [['material_id'], 'exist', 'skipOnError' => true, 'targetClass' => Material::className(), 'targetAttribute' => ['material_id' => 'id']],
            [['sucursal_id'], 'exist', 'skipOnError' => true, 'targetClass' => Sucursal::className(), 'targetAttribute' => ['sucursal_id' => 'id']],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['updated_by' => 'id']]
        ];
    }
	
	public function behaviors(){
		return [
			'timestamp' => [
	            'class' => TimestampBehavior::className(),
	            'attributes' => [
	                ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
	                ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
	            ],
	            'value' => new Expression('NOW()'),
             ],
			'blameable' => ['class' => BlameableBehavior::className(),],
        ];
	}

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'material_id' => Yii::t('app', 'Material ID'),
            'grosor_id' => Yii::t('app', 'Grosor ID'),
            'area' => Yii::t('app', 'Area'),
            'nueva' => Yii::t('app', 'Nueva'),
            'programada' => Yii::t('app', 'Programada'),
            'sucursal_id' => Yii::t('app', 'Sucursal ID'),
            'almacen_id' => Yii::t('app', 'Almacen ID'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'status' => Yii::t('app', 'Status'),
            'forma' => Yii::t('app', 'Forma'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAlmacen()
    {
        return $this->hasOne(Almacen::className(), ['id' => 'almacen_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGrosor()
    {
        return $this->hasOne(Grosor::className(), ['id' => 'grosor_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMaterial()
    {
        return $this->hasOne(Material::className(), ['id' => 'material_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSucursal()
    {
        return $this->hasOne(Sucursal::className(), ['id' => 'sucursal_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLaminaCortes()
    {
        return $this->hasMany(LaminaCorte::className(), ['lamina_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLaminaRetals()
    {
        return $this->hasMany(LaminaRetal::className(), ['lamina_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return \frontend\modules\corte\modelsQuery\LaminaQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \frontend\modules\corte\modelsQuery\LaminaQuery(get_called_class());
    }
}
