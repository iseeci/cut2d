<?php

namespace frontend\modules\corte\controllers;

use Yii;
use frontend\modules\corte\models\Lamina;
use frontend\modules\corte\models\LaminaSearch;
use frontend\modules\inventario\models\Material;
use frontend\modules\inventario\models\Grosor;
use common\controllers\BasicController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/**
 * LaminaController implements the CRUD actions for Lamina model.
 */
class LaminaController extends BasicController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Lamina models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new LaminaSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Lamina model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Lamina model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Lamina();
		
		$materiales=ArrayHelper::map(Material::find()->where(['status'=>'activo'])->all(),'id','titulo');
		asort($materiales);
		
		$grosores=ArrayHelper::map(Grosor::find()->where(['status'=>'activo'])->all(),'id','titulo');
		asort($grosores);
		
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'materiales' => $materiales,
                'grosores' => $grosores,
            ]);
        }
    }

    /**
     * Creates new Lamina models.
     * If creation is successful, the browser will be redirected to the 'index' page.
     * @return mixed
     */
    public function actionCreateBatch()
    {
        $model[0]=new Lamina();

        $rowViewPath='/lamina/_formBatchRow';
		
        if(isset(Yii::$app->request->post()['Lamina']))
        {//var_dump($_POST['ProgramacionChecklist']);die;	
            $success=1;
            foreach($_POST['Lamina'] as $k=>$i){
            	if(is_numeric($k)){
	                $model[$k]=new Lamina();
	                $model[$k]->attributes=$i;
	                if(!$model[$k]->validate())
	                    {$success=0;}
				}
            }
            if($success){
            	foreach($model as $m){
					
                }
            }
            if($success!=0)
                return $this->redirect(array('index'));
        }
		
        return $this->render('createBatch',array(
            'model'=>$model,
            'rowViewPath'=>$rowViewPath,
        ));
    }
	
    public function actionRow($n)
    {
        $model=new Lamina();
        $form=new ActiveForm;
        return $this->renderPartial('_formBatchRow',['model'=>$model,'n'=>$n,'form'=>$form]);
    }

    /**
     * Updates an existing Lamina model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Lamina model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Lamina model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Lamina the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Lamina::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
