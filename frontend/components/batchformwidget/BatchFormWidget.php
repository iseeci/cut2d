<?php
namespace frontend\components\batchformwidget;

use yii\base\Widget;
use yii\helpers\Url;
use yii\web\BadRequestHttpException;

class BatchFormWidget extends Widget{
    public $rowViewPath;
	public $rowContainerId='row-container';
	public $models=[];
    public $rowViewParams=[]; 
	
    public function init(){
        parent::init();
        if($this->models===null){
            throw new BadRequestHttpException(\Yii::t('app','Debes especificar un arreglo de modelos'));
        }
        if($this->rowViewPath===null){
            throw new BadRequestHttpException(\Yii::t('app','Debes especificar un archivo para la vista de línea'));
        }
    }
	
    public function run(){
        return $this->render('ajaxUrlContainer',['widget'=>$this]);
    }
}
?>
