<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>

<div class="pull-right">
	<?php echo Html::a(\Yii::t('app','Nueva Línea'),''); ?>
</div>

<div class="clearfix"></div>
<?php $form = ActiveForm::begin(); ?>
	<div id="batch-container">
		<?php 
			$widget->rowViewParams+=['form'=>$form];
			foreach($widget->models as $k=>$m){
				echo $this->render($widget->rowViewPath, ['params'=>$widget->rowViewParams+['model'=>$m,'n'=>$k]]);
			}
		?>
	</div>
<?php ActiveForm::end(); ?>