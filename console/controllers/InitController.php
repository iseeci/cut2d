<?php
namespace console\controllers;

use Yii;
use yii\console\Controller;
use yii\db\Connection;
use frontend\modules\user\models\ConsoleUser;
use dektrium\user\Finder;
use dektrium\user\models\Token;
use frontend\models\DireccionParametro;

class InitController extends Controller
{
	public $admin_id=1, $ingeniero1_id=3, $ingeniero2_id=4, $ingeniero3_id=5, $manager1_id=6, $manager2_id=7;
	public $colombia_id=1, $antioquia_id=1, $medellin_id=1, $cundinamarca_id=2, $bogota_id=2, $valle_id=3, $cali_id=3;
	
	public function actionFull(){
		$this->actionUsersRbac();
		$this->actionAppPars();
		$this->actionGeo();
		$this->actionInventario();
	}
	
	public function actionFullDummy(){
		$this->actionFull();
	}
	
	public function actionUsersRbac(){
    	$cn=\Yii::$app->db;
		
		echo 'Inicializando tablas de usuarios'.PHP_EOL;
		$cn->createCommand('set foreign_key_checks=0;
			truncate token;truncate social_account;foreign_key_checks=1;')->execute();
		$cn->createCommand('set foreign_key_checks=0;truncate social_account;foreign_key_checks=1;')->execute();
		$cn->createCommand('set foreign_key_checks=0;truncate profile;foreign_key_checks=1;')->execute();
		$cn->createCommand('set foreign_key_checks=0;truncate user;foreign_key_checks=1;')->execute();
		
		echo 'Registrando administrador'.PHP_EOL;
		$fernandrez= new ConsoleUser(['scenario'=>'register']);
		$fernandrez->setAttributes([
            'email'    => 'fernandrez@gmail.com',
            'username' => 'fernandrez',
            'password' => 'fernandrez2015'
        ]);
		$fernandrez->register();
		$fernandrez->attemptConfirmation($fernandrez->code);
		
		echo 'Inicializando tablas de autenticacion'.PHP_EOL;
		$cn->createCommand('set foreign_key_checks=0;truncate auth_item_child;
			truncate auth_assignment;
			truncate auth_item;
			truncate auth_rule;
			foreign_key_checks=1;')->execute();
        $auth = Yii::$app->authManager;
		
		echo 'Creando permisos'.PHP_EOL;
        //Controladores y sus acciones
		$actions['base']=['index','view'];
        $actions['geo']['pais']=['update','create','delete'];
        $actions['geo']['region']=['update','create','delete','get-regiones-pais'];
        $actions['geo']['ciudad']=['update','create','delete','get-ciudades-region'];
        $actions['corte']['lamina']=['create','update','delete','create-batch','row'];
        
        $modules=array_filter(array_keys($actions),function($v){return $v!='base';});
		
		foreach($modules as $m){
			$controllers=array_keys($actions[$m]);
			foreach($controllers as $c){
				$actions[$m][$c]=array_merge($actions['base'],isset($actions[$m][$c])?$actions[$m][$c]:[]);
			}
		}
		
        $fcp='full-controller-permission';
		foreach($modules as $m){
			$controllers=array_keys($actions[$m]);
			foreach($controllers as $c){
				$permissions[$m][$c][$fcp]=$auth->createPermission($m.'-'.$c.'-'.$fcp);
				$permissions[$m][$c][$fcp]->description = $m.'-'.$c.'-'.$fcp;
				$auth->add($permissions[$m][$c][$fcp]);
				foreach($actions[$m][$c] as $a){
					$permissions[$m][$c][$a]=$auth->createPermission($m.'-'.$c.'-'.$a);
					$permissions[$m][$c][$a]->description = $m.'-'.$c.'-'.$a;
					$auth->add($permissions[$m][$c][$a]);
					$auth->addChild($permissions[$m][$c][$fcp],$permissions[$m][$c][$a]);
				}
			}		
		}		
		
		echo 'Creando roles'.PHP_EOL;
        $adminRole = $auth->createRole('admin');
        $auth->add($adminRole);
		
		echo 'Asignando permisos a rol admin'.PHP_EOL;
        foreach($permissions as $m=>$permission){
        	foreach($permission as $c=>$p){
        		$auth->addChild($adminRole, $p[$fcp]);
			}
		}
		
		echo 'Asignando roles a usuarios'.PHP_EOL;
		//Admin
        $auth->assign($adminRole, $fernandrez->id);$this->admin_id=$fernandrez->id;
    }

	public function actionAppPars(){
    	$cn=\Yii::$app->db;
		
		echo 'Inicializando tabla de parametros de aplicacion'.PHP_EOL;
		$cn->createCommand('set foreign_key_checks=0;truncate parametro;foreign_key_checks=1;')->execute();
		$fields=[];
		$data=[];
		//$this->batchInsert('frontend\models\Parametro',$fields,$data);
	}

	public function actionGeo(){
    	$cn=\Yii::$app->db;
		
		echo 'Inicializando tablas de geografia'.PHP_EOL;
		$cn->createCommand('set foreign_key_checks=0;truncate ciudad;truncate region;truncate pais;foreign_key_checks=1;')->execute();
		$fields=['pais_cd', 'nombre', 'created_by'];
		$data=[
			['CO','Colombia',$this->admin_id],
		];
		
		echo 'Insertando paises'.PHP_EOL;
		$ids_paises=$this->batchInsert('frontend\modules\geo\models\Pais',$fields,$data);
		
		$this->colombia_id=$ids_paises[0];
		
		$fields=['pais_id', 'region_cd', 'nombre', 'created_by'];
		$data=[
			[$this->colombia_id, 'ANT','Antioquia',$this->admin_id],
			[$this->colombia_id, 'CUN','Cundinamarca',$this->admin_id],
			[$this->colombia_id, 'VAL','Valle',$this->admin_id],
		];
		
		echo 'Insertando regiones'.PHP_EOL;
		$ids_regiones=$this->batchInsert('frontend\modules\geo\models\Region',$fields,$data);
		
		
		$this->antioquia_id=$ids_regiones[0];
		$this->cundinamarca_id=$ids_regiones[1];
		$this->valle_id=$ids_regiones[2];
		
		$fields=['pais_id', 'region_id', 'nombre', 'created_by'];
		$data=[
			[$this->colombia_id, $this->antioquia_id,'Medellín',$this->admin_id],
			[$this->colombia_id, $this->antioquia_id,'Envigado',$this->admin_id],
			[$this->colombia_id, $this->antioquia_id,'Itagüí',$this->admin_id],
			[$this->colombia_id, $this->antioquia_id,'Sabaneta',$this->admin_id],
			[$this->colombia_id, $this->antioquia_id,'Bello',$this->admin_id],
			[$this->colombia_id, $this->cundinamarca_id,'Bogotá',$this->admin_id],
			[$this->colombia_id, $this->cundinamarca_id,'Chía',$this->admin_id],
			[$this->colombia_id, $this->valle_id,'Cali',$this->admin_id],
		];
		
		echo 'Insertando ciudades'.PHP_EOL;
		$ids_ciudades=$this->batchInsert('frontend\modules\geo\models\Ciudad',$fields,$data);
		
		$this->medellin_id=$ids_ciudades[0];
		$this->bogota_id=$ids_ciudades[1];
		$this->cali_id=$ids_ciudades[2];
	}

	public function actionInventario(){
    	$cn=\Yii::$app->db;
		echo 'Inicializando tabla de materiales'.PHP_EOL;
		$cn->createCommand('set foreign_key_checks=0;truncate material;foreign_key_checks=1;')->execute();
		$fields=['codigo', 'titulo', 'descripcion', 'created_by'];
		$data=[
			['vidrio-claro','Vidrio Claro','Vidrio Claro. Se ve a través de él.',$this->admin_id],
			['vidrio-opaco','Vidrio Opaco','Vidrio Opaco. No se ve a través de él.',$this->admin_id],
		];
		echo 'Insertando materiales'.PHP_EOL;
		$this->batchInsert('frontend\modules\inventario\models\Material',$fields,$data);
		
		echo 'Inicializando tabla de grosores'.PHP_EOL;
		$cn->createCommand('set foreign_key_checks=0;truncate grosor;foreign_key_checks=1;')->execute();
		$fields=['codigo', 'titulo', 'descripcion', 'created_by'];
		$data=[
			['4mm','4 milímetros','4 milímetros.',$this->admin_id],
			['6mm','6 milímetros','6 milímetros.',$this->admin_id],
		];
		echo 'Insertando grosores'.PHP_EOL;
		$this->batchInsert('frontend\modules\inventario\models\Grosor',$fields,$data);
	}

	private function batchInsert($model,$fields,$data){
		$ret=[];
		foreach($data as $d){
			$m=new $model;$setA=[];	
			foreach($fields as $k=>$f){
				$setA[$f]=$d[$k];
			}
			$m->setAttributes($setA);
			$m->detachBehavior('blameable');
			$m->save();
			if(count($m->errors)>0){
				var_dump($m->errors);
				var_dump($m->attributes);die;
			}
			$ret[]=$m->id;
		}
		return $ret; 
	}
}