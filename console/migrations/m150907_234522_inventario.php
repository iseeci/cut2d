<?php

use yii\db\Schema;
use yii\db\Migration;

class m150907_234522_inventario extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
		
		$this->createTable('{{%material}}', [
            'id' => $this->primaryKey(),
            'codigo' => $this->string(20)->notNull()->unique(),
            'titulo' => $this->string(50)->notNull()->unique(),
            'descripcion' => $this->string()->notNull(),
			'created_at' => $this->datetime()->notNull(),
			'updated_at' => $this->datetime(),
			'created_by' => $this->integer()->notNull(),
			'updated_by' => $this->integer(),
			'status' => $this->string()->notNull()->defaultValue('activo')
        ], $tableOptions);

        $this->addForeignKey('fk_material_created_by','{{%material}}','created_by','{{%user}}','id','CASCADE','RESTRICT');
		$this->addForeignKey('fk_material_updated_by','{{%material}}','updated_by','{{%user}}','id','CASCADE','RESTRICT');
		
		$this->createTable('{{%grosor}}', [
            'id' => $this->primaryKey(),
            'codigo' => $this->string(20)->notNull()->unique(),
            'titulo' => $this->string(50)->notNull()->unique(),
            'descripcion' => $this->string()->notNull(),
			'created_at' => $this->datetime()->notNull(),
			'updated_at' => $this->datetime(),
			'created_by' => $this->integer()->notNull(),
			'updated_by' => $this->integer(),
			'status' => $this->string()->notNull()->defaultValue('activo')
        ], $tableOptions);

        $this->addForeignKey('fk_grosor_created_by','{{%grosor}}','created_by','{{%user}}','id','CASCADE','RESTRICT');
		$this->addForeignKey('fk_grosor_updated_by','{{%grosor}}','updated_by','{{%user}}','id','CASCADE','RESTRICT');
		
        $this->createTable('{{%sucursal}}', [
            'id' => $this->primaryKey(),
            'codigo' => $this->integer()->notNull(),
            'titulo' => $this->integer()->notNull(),
            'descripcion' => $this->string(5)->notNull(),
            'pais_id' => $this->integer()->notNull(),
            'pais_cd' => $this->string(5)->notNull(),
            'region_id' => $this->integer()->notNull(),
            'region_cd' => $this->string(5)->notNull(),
            'ciudad_id' => $this->integer()->notNull(),
            'direccion_id' => $this->integer()->notNull(),
			'created_at' => $this->datetime()->notNull(),
			'updated_at' => $this->datetime(),
			'created_by' => $this->integer()->notNull(),
			'updated_by' => $this->integer(),
			'status' => $this->string()->notNull()->defaultValue('activa')
        ], $tableOptions);
		
		$this->addForeignKey('fk_sucursal_ciudad','{{%sucursal}}','ciudad_id','{{%ciudad}}','id','CASCADE','RESTRICT');
		$this->addForeignKey('fk_sucursal_region','{{%sucursal}}','region_id','{{%region}}','id','CASCADE','RESTRICT');
		$this->addForeignKey('fk_sucursal_pais','{{%sucursal}}','pais_id','{{%pais}}','id','CASCADE','RESTRICT');
		$this->addForeignKey('fk_sucursal_direccion','{{%sucursal}}','direccion_id','{{%direccion}}','id','CASCADE','RESTRICT');
		$this->addForeignKey('fk_sucursal_created_by','{{%sucursal}}','created_by','{{%user}}','id','CASCADE','RESTRICT');
		$this->addForeignKey('fk_sucursal_updated_by','{{%sucursal}}','updated_by','{{%user}}','id','CASCADE','RESTRICT');
		
		$this->createTable('{{%almacen}}', [
            'id' => $this->primaryKey(),
            'sucursal_id' => $this->integer()->notNull(),
            'material_id' => $this->integer()->notNull(),
            'grosor_id' => $this->integer()->notNull(),
            'nombre' => $this->string(50)->notNull()->unique(),
            'descripcion' => $this->string()->notNull(),
			'created_at' => $this->datetime()->notNull(),
			'updated_at' => $this->datetime(),
			'created_by' => $this->integer()->notNull(),
			'updated_by' => $this->integer(),
			'status' => $this->string()->notNull()->defaultValue('activo')
        ], $tableOptions);

        $this->addForeignKey('fk_almacen_sucursal','{{%almacen}}','sucursal_id','{{%sucursal}}','id','CASCADE','RESTRICT');
        $this->addForeignKey('fk_almacen_material','{{%almacen}}','material_id','{{%material}}','id','CASCADE','RESTRICT');
		$this->addForeignKey('fk_almacen_grosor','{{%almacen}}','grosor_id','{{%grosor}}','id','CASCADE','RESTRICT');
        $this->addForeignKey('fk_almacen_created_by','{{%almacen}}','created_by','{{%user}}','id','CASCADE','RESTRICT');
		$this->addForeignKey('fk_almacen_updated_by','{{%almacen}}','updated_by','{{%user}}','id','CASCADE','RESTRICT');
	}

    public function down()
    {
        $this->dropTable('{{%almacen}}');
        $this->dropTable('{{%sucursal}}');
        $this->dropTable('{{%material}}');
        $this->dropTable('{{%grosor}}');
    }
}
