<?php

use yii\db\Schema;
use yii\db\Migration;

class m150907_234523_cut2d extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
		
		$this->createTable('{{%lamina}}', [
            'id' => $this->primaryKey(),
            'material_id' => $this->integer()->notNull(),
            'grosor_id' => $this->integer()->notNull(),
            'area' => $this->float()->notNull(),
            'nueva' => $this->boolean()->notNull(),
            'programada' => $this->boolean()->notNull(),
            'sucursal_id' => $this->integer()->notNull(),
            'almacen_id' => $this->integer()->notNull(),
			'created_at' => $this->datetime()->notNull(),
			'updated_at' => $this->datetime(),
			'created_by' => $this->integer()->notNull(),
			'updated_by' => $this->integer(),
			'status' => $this->string()->notNull()->defaultValue('activa')
        ], $tableOptions);
		$this->addColumn('{{%lamina}}','forma','polygon');
		
        $this->addForeignKey('fk_lamina_sucursal','{{%lamina}}','sucursal_id','{{%sucursal}}','id','CASCADE','RESTRICT');
        $this->addForeignKey('fk_lamina_almacen','{{%lamina}}','almacen_id','{{%almacen}}','id','CASCADE','RESTRICT');
        $this->addForeignKey('fk_lamina_material','{{%lamina}}','material_id','{{%material}}','id','CASCADE','RESTRICT');
		$this->addForeignKey('fk_lamina_grosor','{{%lamina}}','grosor_id','{{%grosor}}','id','CASCADE','RESTRICT');
		$this->addForeignKey('fk_lamina_created_by','{{%lamina}}','created_by','{{%user}}','id','CASCADE','RESTRICT');
		$this->addForeignKey('fk_lamina_updated_by','{{%lamina}}','updated_by','{{%user}}','id','CASCADE','RESTRICT');
		
		$this->createTable('{{%pedido}}', [
            'id' => $this->primaryKey(),
            'cliente_id' => $this->integer()->notNull(),
            'fecha_hora_pedido' => $this->datetime()->notNull(),
            'area_total' => $this->float()->notNull(),
            'domicilio' => $this->boolean()->notNull(),
            'direccion_id' => $this->integer(),
			'created_at' => $this->datetime()->notNull(),
			'updated_at' => $this->datetime(),
			'created_by' => $this->integer()->notNull(),
			'updated_by' => $this->integer(),
			'status' => $this->string()->notNull()->defaultValue('inicial')
        ], $tableOptions);
		
        $this->addForeignKey('fk_pedido_cliente','{{%pedido}}','cliente_id','{{%user}}','id','CASCADE','RESTRICT');
        $this->addForeignKey('fk_pedido_direccion','{{%pedido}}','direccion_id','{{%direccion}}','id','CASCADE','RESTRICT');
		$this->addForeignKey('fk_pedido_created_by','{{%pedido}}','created_by','{{%user}}','id','CASCADE','RESTRICT');
		$this->addForeignKey('fk_pedido_updated_by','{{%pedido}}','updated_by','{{%user}}','id','CASCADE','RESTRICT');
		
		$this->createTable('{{%pedido_lamina}}', [
            'id' => $this->primaryKey(),
            'pedido_id' => $this->integer()->notNull(),
            'cliente_id' => $this->integer()->notNull(),
            'fecha_hora_pedido' => $this->datetime()->notNull(),
            'material_id' => $this->integer()->notNull(),
            'grosor_id' => $this->integer()->notNull(),
            'area_unitaria' => $this->float()->notNull(),
            'cantidad' => $this->integer()->notNull(),
			'created_at' => $this->datetime()->notNull(),
			'updated_at' => $this->datetime(),
			'created_by' => $this->integer()->notNull(),
			'updated_by' => $this->integer(),
			'status' => $this->string()->notNull()->defaultValue('inicial')
        ], $tableOptions);
		$this->addColumn('{{%pedido_lamina}}','forma','polygon');
		
		$this->addForeignKey('fk_pedido_lamina_pedido','{{%pedido_lamina}}','pedido_id','{{%pedido}}','id','CASCADE','RESTRICT');
        $this->addForeignKey('fk_pedido_lamina_cliente','{{%pedido_lamina}}','cliente_id','{{%user}}','id','CASCADE','RESTRICT');
        $this->addForeignKey('fk_pedido_lamina_material','{{%pedido_lamina}}','material_id','{{%material}}','id','CASCADE','RESTRICT');
		$this->addForeignKey('fk_pedido_lamina_grosor','{{%pedido_lamina}}','grosor_id','{{%grosor}}','id','CASCADE','RESTRICT');
		$this->addForeignKey('fk_pedido_lamina_created_by','{{%pedido_lamina}}','created_by','{{%user}}','id','CASCADE','RESTRICT');
		$this->addForeignKey('fk_pedido_lamina_updated_by','{{%pedido_lamina}}','updated_by','{{%user}}','id','CASCADE','RESTRICT');
		
		$this->createTable('{{%lamina_corte}}', [
            'id' => $this->primaryKey(),
            'pedido_id' => $this->integer()->notNull(),
            'pedido_lamina_id' => $this->integer()->notNull(),
            'lamina_id' => $this->integer()->notNull(),
            'material_id' => $this->integer()->notNull(),
            'grosor_id' => $this->integer()->notNull(),
            'area' => $this->float()->notNull(),
            'nueva' => $this->boolean()->notNull(),
			'created_at' => $this->datetime()->notNull(),
			'updated_at' => $this->datetime(),
			'created_by' => $this->integer()->notNull(),
			'updated_by' => $this->integer(),
			'status' => $this->string()->notNull()->defaultValue('programada')
        ], $tableOptions);
		$this->addColumn('{{%lamina_corte}}','forma','polygon');
		
		$this->addForeignKey('fk_lamina_corte_pedido','{{%lamina_corte}}','pedido_id','{{%pedido}}','id','CASCADE','RESTRICT');
		$this->addForeignKey('fk_lamina_corte_pedido_lamina','{{%lamina_corte}}','pedido_lamina_id','{{%pedido_lamina}}','id','CASCADE','RESTRICT');
		$this->addForeignKey('fk_lamina_corte_lamina','{{%lamina_corte}}','lamina_id','{{%lamina}}','id','CASCADE','RESTRICT');
        $this->addForeignKey('fk_lamina_corte_material','{{%lamina_corte}}','material_id','{{%material}}','id','CASCADE','RESTRICT');
		$this->addForeignKey('fk_lamina_corte_grosor','{{%lamina_corte}}','grosor_id','{{%grosor}}','id','CASCADE','RESTRICT');
		$this->addForeignKey('fk_lamina_corte_created_by','{{%lamina_corte}}','created_by','{{%user}}','id','CASCADE','RESTRICT');
		$this->addForeignKey('fk_lamina_corte_updated_by','{{%lamina_corte}}','updated_by','{{%user}}','id','CASCADE','RESTRICT');
		
		$this->createTable('{{%lamina_retal}}', [
            'id' => $this->primaryKey(),
            'lamina_corte_id' => $this->integer()->notNull(),
            'lamina_id' => $this->integer()->notNull(),
        ], $tableOptions);
		
		$this->addForeignKey('fk_lamina_retal_lamina_corte','{{%lamina_retal}}','lamina_corte_id','{{%lamina_corte}}','id','CASCADE','RESTRICT');
		$this->addForeignKey('fk_lamina_retal_lamina','{{%lamina_retal}}','lamina_id','{{%lamina}}','id','CASCADE','RESTRICT');
	}

    public function down()
    {
        $this->dropTable('{{%lamina_retal}}');
        $this->dropTable('{{%lamina_corte}}');
        $this->dropTable('{{%lamina}}');
        $this->dropTable('{{%pedido_lamina}}');
		$this->dropTable('{{%pedido}}');
    }
}
